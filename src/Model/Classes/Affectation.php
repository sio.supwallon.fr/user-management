<?php
namespace DWWM\Model\Classes;

use DWWM\Model\Dao\AffectationDao;

class Affectation
{
    public static function affect($id_utilisateur, $id_groupe)
    {
        $dao = new AffectationDao();
        return $dao->affect($id_utilisateur, $id_groupe);
    }

    public static function disaffect($id_utilisateur, $id_groupe)
    {
        $dao = new AffectationDao();
        return $dao->disaffect($id_utilisateur, $id_groupe);
    }
}
