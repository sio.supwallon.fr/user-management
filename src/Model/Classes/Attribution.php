<?php
namespace DWWM\Model\Classes;

use DWWM\Model\Dao\AttributionDao;

class Attribution
{
    // Propriété(s)
    public $id_groupe;
    public $id_privilege;

    // Association(s)
    public $groupe;
    public $privilege;

    // Constructeur
    public function __construct($id_groupe, $id_privilege)
    {
        $this->id_groupe = $id_groupe;
        $this->id_privilege = $id_privilege;
    }

    // Méthodes statiques
    public static function getAll()
    {
        $dao = new AttributionDao();
        return $dao->getAll();
    }

    public static function update($values)
    {
        $dao = new AttributionDao();
        $dao->update($values);
    }
}