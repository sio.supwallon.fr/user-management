        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="<?= $this->path; ?>">Tutorial</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                <?php 
    if(in_array("Administrateur(s)", $this->groupes))
    {
        require "_nav-administrateurs.html.php";
    }
    if(in_array("Comptable(s)", $this->groupes))
    {
        require "_nav-comptables.html.php";
    }
    if(in_array("Directeur(s)", $this->groupes))
    {
        require "_nav-directeurs.html.php";
    }
    if(in_array("Technicien(s)", $this->groupes))
    {
        require "_nav-techniciens.html.php";
    }
    if(in_array("Secrétaire(s)", $this->groupes))
    {
        require "_nav-secretaires.html.php";
    }
?>
                </ul>
<?php require "_nav-login.html.php"; ?>
            </div>
        </nav>
