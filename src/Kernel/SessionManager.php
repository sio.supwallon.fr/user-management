<?php
namespace DWWM\Kernel;

use DWWM\Model\Classes\Utilisateur;
use DWWM\Model\Classes\Groupe;
use DWWM\Model\Classes\Privilege;

class SessionManager
{
    public static function start()
    {
        session_start();
        if (empty($_SESSION))
        {
            // initialisation
            self::init();
        }
        self::check();
    }

    private static function init()
    {
        $_SESSION['connected'] = false;
        $_SESSION['user'] = null;
        $_SESSION['groupes'] = [];
        $_SESSION['privileges'] = [];
    }

    public static function check()
    {
        // si on est connecté
        if (self::isConnected())
        {
            $user = json_decode($_SESSION['user']);

            $login = $user->login;
            $password = $user->password;
    
            $user = Utilisateur::getUtilisateur($login, $password);
            if ($user)
            {
                // on a récupéré un object Utilisateur
                $_SESSION['connected'] = true;
                $_SESSION['user'] = json_encode($user);
                $_SESSION['groupes'] = [];
                $_SESSION['privileges'] = [];

                // On veut récupérer les groupes
                $groupes = Groupe::getGroupesByUtilisateur($user->id);
                // On stocke les groupes dans la session
                foreach ($groupes as $groupe)
                {
                    $_SESSION['groupes'][] = $groupe->nom;
                }

                // On veut récupérer les privilèges
                $privileges = Privilege::getPrivilegesByUtilisateur($user->id);
                // On stocke les privilèges dans la session
                foreach ($privileges as $privilege)
                {
                    $_SESSION['privileges'][] = $privilege->nom;
                }
            }
            else
            {
                // erreur de login ou de password => déconnexion
                self::disconnect();
            }
        }
        else
        {
            // session is disconnected, nothing to do
        }
    }

    public static function disconnect()
    {
        session_destroy();
        session_unset();
        self::start();
    }

    public static function connect($user)
    {
        $_SESSION['user'] = json_encode($user);
        $_SESSION['connected'] = true;
        self::check();
    }

    public static function getUser()
    {
        return json_decode($_SESSION['user']);
    }

    public static function getGroupes()
    {
        return $_SESSION['groupes'];
    }

    public static function getPrivileges()
    {
        return $_SESSION['privileges'];
    }

    public static function isConnected()
    {
        return $_SESSION['connected'];
    }

    public static function hasPrivileges($searched, $strict = false)
    {
        $privileges = self::getPrivileges();

        // échappement du caractère /
        $searched = str_replace("/","\/",$searched);
        
        if ($strict)
        {
            // /^  => marqueur de début de la chaîne
            // $/  => marqueur de fin de la chaîne
            $pattern = "/^" . $searched . "$/";
        }
        else 
        {
            $pattern = "/^" . $searched . "/";
        }

        $result = [];

        foreach ($privileges as $privilege) {
            preg_match($pattern, $privilege, $matches);
            if (! empty($matches))
            {
                $result[] = $privilege;
            }
        }

        return $result;
    }
}